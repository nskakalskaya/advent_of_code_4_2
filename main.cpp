#include <iostream>
#include <string>
#include <algorithm>

constexpr uint32_t begin_range{256310};
constexpr uint32_t end_range{732736};

const bool IsMeetCriteria(std::uint32_t number)
{

    /* Criteria to match:
     *  is_not_decreasing - if digits in this number are not decreasing;
     *  is_found_doubles - if number contains at least two repeated adjacent digits;
     *  is_only_one_double_found - if we found a pair of repeated digits that is not a part of a larger group of repeated digits;
     *  doubles_is_a_part - if number has at least one pair of repeated digits than IS a part of a larger group.
     */
    auto is_not_decreasing{true};
    auto is_found_doubles{false};
    auto is_only_one_double_found{false};
    auto doubles_is_a_part{false};

    auto origin = number;

    std::uint16_t num_of_doubles{0U};

    /*
     * Get the last digit
     */
    std::int64_t last_digit{number % 10};
    number /= 10;

    while (number > 0U)
    {
        /*
         * If digits in number is not increasing than break
         */
         if (last_digit < number % 10)
         {
             is_not_decreasing = false;
             break;
         }
         /*
          * If found at least two repeated digits
          */
         else if (last_digit == number % 10)
         {
             is_found_doubles = true;
             num_of_doubles ++;
         }
         /*
          * Check how many digits we previously found as repeated
          */
         else
         {
             if (num_of_doubles >= 1)
             {
                 /*
                  * If only two digits repeated than we can be sure that this number match our criteria (its part about repeated digits)
                  */
                 if (num_of_doubles == 1)
                 {
                     is_only_one_double_found = true;
                 }
                 /*
                  * If there were more than two repeated digits than this number does not
                  * okay for us unless we find another pair of repeated digits
                  */
                 else
                 {
                     doubles_is_a_part = true;
                 }
             }
             num_of_doubles = 0;
         }

         last_digit = number % 10;
         number /= 10;
    }

    if (num_of_doubles >= 1)
    {
        if (num_of_doubles == 1)
            is_only_one_double_found = true;
        else
            doubles_is_a_part = true;
    }
    return is_not_decreasing && is_found_doubles && (is_only_one_double_found || (!doubles_is_a_part));
}

const uint32_t FindNumberOfPasswords()
{
    std::uint32_t count_passwords{0U};

    for (auto number{begin_range}; number<= end_range; ++number)
    {
        if (IsMeetCriteria(number))
        {
            count_passwords++;
        }
    }
    return count_passwords;
}


int main()
{
    try
    {
        /* https://adventofcode.com/2019/day/4
         * The goal is to find the amount of numbers that lay in range
         * between begin_range and end_range and
         * have no decrease in digits from left to right
         * and have one or more adjecent doubles like 11113, 234558, etc.
         * ALSO
         * Now this pair of adjecent doubles shouldn't be the part of
         * larger group of matching digits like 123444 is not okay because
         * pair 44 is a part of a triplet 444.
        */
        std::cout << FindNumberOfPasswords() << std::endl;
    }
    catch (const std::exception &e)
    {
        std::cout << "Caught exception " << e.what() << std::endl;
        return -1;
    }
    return 0;
}
